#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

from typing import IO, List

cache = [0] * 10000000
cache[1] = 1


# ------------
# collatz_read
# ------------

def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

# Recursive function that returns the cycle length of a single number and
# adds it to the cache if needed


def solve_cycle(val: int) -> int:
    assert(val > 0)
    cycle = 0
    if(val < len(cache) and cache[int(val)] != 0):
        return cache[int(val)]
    elif(val % 2 == 1):
        cycle = solve_cycle((val * 3 + 1) / 2) + 2
    else:
        cycle = solve_cycle(val / 2) + 1

    if(val < len(cache)):
        cache[int(val)] = cycle
    assert(cycle > 0)
    return cycle


def collatz_eval(i: int, j: int) -> None:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert(i > 0)
    assert(j > 0)

    max_cycle = 0
    cycle = 1

    saved_i = i
    saved_j = j

    # Set j to be the larger of i and j for simplicity
    if(j < i):
        temp = i
        i = j
        j = temp

    # Minimilizing i to be j/2 if its more efficient
    i = int(max(i, j / 2))

    assert(j >= i)
    low = int((i - 1) / 100000) + 1
    high = int((j) / 100000)
    if(low <= high):
        # supercache holds the max of every in the range of 100,000
        supercache = [351, 383, 443, 441, 449, 470, 509, 504, 525, 507]
        # Find the max of every range possible
        for num in range(low, high):
            max_cycle = max(max_cycle, supercache[num])
        # Break the remaining chunks into low_i low_i and i, j
        low_i = i
        i = high * 100000
        low_j = low * 100000
        for val in range(low_i, low_j + 1):
            max_cycle = max(max_cycle, solve_cycle(val))

    for val in range(i, j + 1):
        max_cycle = max(max_cycle, solve_cycle(val))

    assert(max_cycle > 0)
    return max_cycle

# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
